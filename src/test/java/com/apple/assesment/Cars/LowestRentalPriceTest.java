package com.apple.assesment.Cars;

import java.util.Collections;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.jayway.restassured.path.json.JsonPath;

//Question 2: Return all cars which have the lowest per day rental cost for both cases:
//	a. Price only
//	b. Price after discounts
public class LowestRentalPriceTest {

	CarRepository cars;
	JsonPath jsRes;

	@BeforeClass
	void Initilize() {
		cars = new CarRepository();
		/* store return response as json */
		jsRes = cars.returnResponseJsonPath();

	}

	/* price only */
	@Test
	public void priceOnly() {

		/* get car which has minimum price */
		List<Float> listOfPriceWithDiscount = jsRes.get("Car.perdayrent.Price");
		Float minPrice = Collections.min(listOfPriceWithDiscount);

		/* print car details which has minimum price */
		System.out.println("Details of Car whose rent is lowest : "
				+ jsRes.get("Car.findAll{it->it.perdayrent.Price == " + minPrice + "}"));
		System.out.println();
	}

	/* price after discount */
	@Test
	public void priceAfterDiscount() {
		
		/* call reusable method discountPrice and store it in a list */
		List<Float> listOfPriceWithDiscount = ReusableMethods.getDiscountPrices(jsRes);

		/* index of minimum price with discount */
		int minPriceIndex = listOfPriceWithDiscount.indexOf(Collections.min(listOfPriceWithDiscount));
		
		/* print car details with minimum price with discount */
		System.out.println("Car details with minimum price with discount");
		System.out.println(jsRes.get("Car[" + minPriceIndex + "]"));
		System.out.println();
	}

}