package com.apple.assesment.Cars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.jayway.restassured.path.json.JsonPath;

public class ReusableMethods {

	public static List<Float> getDiscountPrices(JsonPath jsRes) {
		/* get list of price and store it in listOfPrice */

		List<Float> listOfPrice = jsRes.get("Car.perdayrent.Price");

		/* get list of discount and store it in listOfDiscount */
		List<Float> listOfDiscount = jsRes.get("Car.perdayrent.Discount");

		/* create array list to store price with discount */
		List<Float> listOfPriceWithDiscount = new ArrayList<Float>();

		for (int i = 0; i < listOfPrice.size(); i++) {
			listOfPriceWithDiscount
					.add(listOfPrice.get(i) - (listOfPrice.get(i) * listOfDiscount.get(i)) / 100);
		}
		return listOfPriceWithDiscount;
	}
	
	/* Returns Index for Highest Revence */
	public static Integer getIndexOfCarWithHighestRevenue(JsonPath jsRes) {
		List<Float> listOfProfit = new ArrayList<Float>();
		
		/* get list of price */
		List<Float> listOfPrice = jsRes.get("Car.perdayrent.Price");
		
		/* get list of mainatenance */
		List<Float> listOfMaintanceCost = jsRes.get("Car.metrics.yoymaintenancecost");
		
		/* get list of depreciation */
		List<Float> listOfDepreciation = jsRes.get("Car.metrics.depreciation");
		
		/* get list of rental count year to date */
		List<Integer> listOfRentedDays = jsRes.get("Car.metrics.rentalcount.yeartodate");

		/* calculate discountedPrice */
		List<Float> listOfDiscountedPrice = getDiscountPrices(jsRes);
		
		/*
		 * add values to listOfProfit after applying (pricewithdiscount
		 * *rentalcount)-(maintanceCost + depreciation)
		 */
		for (int i = 0; i < listOfPrice.size(); i++) {
			listOfProfit.add((listOfDiscountedPrice.get(i) * listOfRentedDays.get(i))
					- (listOfMaintanceCost.get(i) + listOfDepreciation.get(i)));
		}
		/* find highest revenue car index */
		return listOfProfit.indexOf(Collections.max(listOfProfit));

	}
}