package com.apple.assesment.Cars;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.jayway.restassured.path.json.JsonPath;

public class CarRepository {
	Object cars;

	/* Extract response as string and return response */
	public String returnResponseString() {
		/* when calling actual Server */
		/*
		 * String res = RestAssured.given() .relaxedHTTPSValidation() .when()
		 * .get("https://Cars.com/cars") .then() .extract() .response().asString();
		 * return res;
		 */
		
		/* when calling Mock JSON */
		try {
			String parent = new File("").getAbsolutePath();
			cars = new JSONParser().parse(new FileReader(parent + "/src/test/java/Cars.json"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return cars.toString();
	}
	
	/* return response as JSON path */
	public JsonPath returnResponseJsonPath() {
		/* store return response in a string */
		String res = returnResponseString();
		/* convert response string to JSON */
		return new JsonPath(res);
	}
}
