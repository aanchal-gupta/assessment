package com.apple.assesment.Cars;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.jayway.restassured.path.json.JsonPath;

public class HighestProfitCarTest {
	CarRepository cars;
	JsonPath jsRes;

	@BeforeClass
	void Initilize() {
		cars = new CarRepository();
		/* store return response as json */
		jsRes = cars.returnResponseJsonPath();
	}

	/* Calculate highest revenue car */
	@Test
	public void findHighestProfitCar() {
		
		/* find highest revenue car index */
		Integer index = ReusableMethods.getIndexOfCarWithHighestRevenue(jsRes);

		/* print highest car revenue */
		System.out.println("Car with highest revenue");
		System.out.println(jsRes.get("Car[" + index + "]"));
		System.out.println();
	}
}