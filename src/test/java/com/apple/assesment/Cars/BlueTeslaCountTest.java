package com.apple.assesment.Cars;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;
import com.jayway.restassured.path.json.JsonPath;

//Question 1: Print all the blue Teslas received in the web service response.

public class BlueTeslaCountTest {
	
	CarRepository cars;
	JsonPath jsRes;

	@BeforeClass
	void Initilize() {
		cars = new CarRepository();
		/* store return response as json */
		jsRes = cars.returnResponseJsonPath();
	}

	/*
	 * Print all the blue Tesla received in the web service response. Also print
	 * the notes
	 */
	@Test
	public void printBlueTesla() throws IOException {
		
		// Correct Blue Tesla Count
		Integer expected =2 ,actual =0;
		
		/* get list of cars from the response */
		int count = jsRes.get("Car.size()");
		/*
		 * traverse through the loop and check model = tesla and color = blue and print
		 * model and notes
		 */
		System.out.println("All the Tesla Blue cars");
		for (int i = 0; i < count; i++) {
			if ((jsRes.get("Car[" + i + "].make").equals("Tesla"))
					&& (jsRes.get("Car[" + i + "].metadata.Color").equals("Blue"))) {
				System.out.println("Car Model: " + jsRes.get("Car[" + i + "].make"));
				System.out.println("Car Notes: " + jsRes.get("Car[" + i + "].metadata.Notes"));
				actual++;
			}
		}
		if(actual ==0) {
			System.out.println("None");
		}
		System.out.println();
		Assert.assertEquals(actual, expected);
	}
}
